import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { LayoutModule } from 'angular-admin-lte';    // Loading layout module
import { BoxModule, BoxInfoModule, BreadcrumbsModule } from 'angular-admin-lte';       // Box component
import { adminLteConf } from './admin-lte.conf';   // Import the layout configuration.
import {MatInputModule, MatIconModule, MatButtonModule, MatTableModule, MatDialogModule, MatSelectModule } from '@angular/material';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';
import { ToastyModule } from 'ng2-toasty';
import { NgxLoadingModule } from 'ngx-loading';

import { AuthGuard } from './components/_guard/auth.guard';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { LoginComponent } from './components/login/login.component';
import { ProductsComponent } from './components/products/products.component';
import { BlogsComponent } from './components/blogs/blogs.component';
import { ProductsViewComponent } from './components/products/products-view.component';
import { DialogComponent } from './components/dialog/dialog.component';
import { CategoryComponent } from './components/category/category.component';
import { CategoriesViewComponent } from './components/category/category-view.component';

@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    LoginComponent,
    DialogComponent,
    ProductsComponent,
    BlogsComponent,
    ProductsViewComponent,
    CategoryComponent,
    CategoriesViewComponent
  ],
  imports: [
    BrowserAnimationsModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    HttpModule,
    BrowserModule,
    AppRoutingModule,
    MatInputModule,
    LayoutModule.forRoot(adminLteConf),
    BoxModule,
    BoxInfoModule, BreadcrumbsModule,
    MatIconModule,
    MatButtonModule,
    ToastyModule,
    NgxLoadingModule,
    MatTableModule,
    MatDialogModule,
    MatSelectModule
  ],
  providers: [AuthGuard],
  bootstrap: [AppComponent],
  entryComponents: [DialogComponent]
})
export class AppModule { }

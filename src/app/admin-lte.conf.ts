// tslint:disable-next-line:no-var-keyword
export var adminLteConf = {
    skin: 'purple-light',
    isSidebarLeftMini: false,
    sidebarLeftMenu: [
      {label: 'Dashboard', route: '/dashboard', iconClasses: 'fa fa-th'},
      {label: 'Products', iconClasses: 'fa fa-th', children: [
        { label: 'Add', route: '/product', iconClasses: 'fa fa-th' },
        { label: 'View Products', route: '/product-view', iconClasses: 'fa fa-th' }
    ]},
    {label: 'Categories', iconClasses: 'fa fa-th', children: [
      { label: 'Add', route: '/category', iconClasses: 'fa fa-th' },
      { label: 'View Categories', route: '/category-view', iconClasses: 'fa fa-th' }
    ]},
    ]
  };

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './components/login/login.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProductsComponent } from './components/products/products.component';
import { BlogsComponent } from './components/blogs/blogs.component';
import { ProductsViewComponent } from './components/products/products-view.component';
import { AuthGuard } from './components/_guard/auth.guard';
import { CategoriesViewComponent } from './components/category/category-view.component';
import { CategoryComponent } from './components/category/category.component';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/dashboard' },
  {
    path: 'dashboard',
    component : DashboardComponent,
    data: {
      title: 'Dashboard',
      description: 'Home',
      breadcrumbs: 'Dashboard'
    },
     canActivate: [AuthGuard]
  },
  { path: 'product', component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'product/:ID', component: ProductsComponent, canActivate: [AuthGuard] },
  { path: 'product-view', component: ProductsViewComponent, canActivate: [AuthGuard]},
  { path: 'category', component: CategoryComponent, canActivate: [AuthGuard] },
  { path: 'category/:ID', component: CategoryComponent, canActivate: [AuthGuard] },
  { path: 'category-view', component: CategoriesViewComponent, canActivate: [AuthGuard]},
  { path: 'blog', component: BlogsComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent }
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, Inject, ViewChild, ElementRef, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ProductFragrance } from '../products/models/product.model';

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.css']
})


export class DialogComponent implements OnInit {
    @ViewChild('fileInput') myFileInput: ElementRef;
    previewURL: any;
    ProductFragrance: ProductFragrance = new ProductFragrance();
    /** dialog ctor */
    constructor(
        public dialogRef: MatDialogRef<DialogComponent>,
        @Inject(MAT_DIALOG_DATA) public data: any) {
        }

    ngOnInit() {
        if (this.data.isAdd === false) {
            this.ProductFragrance.fileName = this.data.fragrance.fileName;
            this.ProductFragrance.fragranceName = this.data.fragrance.fragranceName;
            console.log(this.ProductFragrance);
        }
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    onSelectFile(event) {
        if (event.target.files && event.target.files[0]) {
            const filesAmount = event.target.files.length;
            for (let i = 0; i < filesAmount; i++) {
                    const reader = new FileReader();
                    reader.onload = (events: any) => {
                      this.previewURL = events.target.result;
                      this.ProductFragrance.fileName = events.target.result;
                    //   this.ProductFragrance.fileName = events.target.result.toString().split(',')[1];
                    };
                    reader.readAsDataURL(event.target.files[i]);
            }
        }
      }
}

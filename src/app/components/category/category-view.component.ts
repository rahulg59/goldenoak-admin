import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Product } from '../products/models/product.model';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import { MatTableDataSource } from '@angular/material';
import { ProductsService } from '../products/services/product.service';

@Component({
  selector: 'app-category-view',
  templateUrl: './category-view.component.html',
  styleUrls: ['./category.component.css'],
  providers: [ProductsService, ToastyService, ToastyConfig]
})
export class CategoriesViewComponent implements OnInit {
  loading = false;
  displayedColumns: string[] = ['position', 'name', 'action'];
  dataSource: any;
  constructor(
    private fb: FormBuilder,
    private productService: ProductsService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
    ) {
      this.toastyConfig.theme = 'material';
      this.toastyConfig.position = 'center-center';
    }

  ngOnInit() {
    this.loading = true;
    this.productService.GetAllCategories().subscribe(
      data => {
        this.loading = false;
        this.dataSource = new MatTableDataSource(data);
      }
    );
  }

  deleteProduct(id: any) {
    this.loading = true;
    this.productService.DeleteCategory(id).subscribe(
      result => {
        this.loading = false;
        this.toastyService.success('Product Deleted');
        this.dataSource = null;
        this.loading = true;
        this.productService.GetAllCategories().subscribe(
          data => {
            this.loading = false;
            this.dataSource = new MatTableDataSource(data);
          }
        );
      }
    );
  }




}

import { Component, OnInit } from '@angular/core';
import { Category } from '../products/models/product.model';
import { ProductsService } from '../products/services/product.service';
import { ToastyConfig, ToastyService } from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.css'],
  providers: [ProductsService, ToastyConfig, ToastyService]
})
export class CategoryComponent implements OnInit {
  Category: Category = new Category();
  loading = false;
  ID: any;
  constructor(
    private route: Router,
    private activatedRoute: ActivatedRoute,
    private productService: ProductsService,
    private toastyConfig: ToastyConfig,
    private toastyService: ToastyService
    ) {
      this.toastyConfig.theme = 'material';
      this.toastyConfig.position = 'center-center';
    }

  ngOnInit() {
    this.ID = this.activatedRoute.snapshot.paramMap.get('ID');
    if (this.ID != null) {
      this.loading = true;
      this.productService.GetCategorybyId(this.ID).subscribe(
        data => {
          this.loading = false;
          this.Category = data;
        }
      );
    }
  }
  submit() {
    this.loading = true;
    this.productService.AddCategory(this.Category).subscribe(
      data => {
        this.loading = false;
        this.toastyService.success('Category Saved');
        this.route.navigate(['/category-view']);
      }
    );
  }

}

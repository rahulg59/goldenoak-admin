export class AppConfig {

    // public readonly apiUrl = 'https://localhost:44370/api/';
    // public readonly ImageUrl = 'https://localhost:44370/images/';
    public readonly apiUrl = 'http://recreationlife-001-site15.dtempurl.com/api/';
    public readonly ImageUrl = 'http://recreationlife-001-site15.dtempurl.com/images/';

}

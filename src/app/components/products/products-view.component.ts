import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Product } from './models/product.model';
import { ProductsService } from './services/product.service';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import { MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-products-view',
  templateUrl: './products-view.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductsService, ToastyService, ToastyConfig]
})
export class ProductsViewComponent implements OnInit {
  loading = false;
  displayedColumns: string[] = ['position', 'name', 'action'];
  dataSource: any;
  constructor(
    private fb: FormBuilder,
    private productService: ProductsService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig
    ) {
      this.toastyConfig.theme = 'material';
      this.toastyConfig.position = 'center-center';
    }

  ngOnInit() {
    this.loading = true;
    this.productService.GetAllProduct().subscribe(
      data => {
        this.loading = false;
        this.dataSource = new MatTableDataSource(data);
      }
    );
  }

  deleteProduct(id: any) {
    this.loading = true;
    this.productService.DeleteProduct(id).subscribe(
      result => {
        this.loading = false;
        this.toastyService.success('Product Deleted');
        this.dataSource = null;
        this.loading = true;
        this.productService.GetAllProduct().subscribe(
          data => {
            this.loading = false;
            this.dataSource = new MatTableDataSource(data);
          }
        );
      }
    );
  }




}

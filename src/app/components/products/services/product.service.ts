import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/add/observable/throw';
import { AppConfig } from '../../_config/app.config';
import { Product, ProductFragrance, Category } from '../models/product.model';

@Injectable()
export class ProductsService {

    public apiUrl: string;

    constructor(private http: Http, private route: Router) {
        this.apiUrl = new AppConfig().apiUrl;
    }

    GetProductbyId(id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetFragranceProductbyId(id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/fragrance/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetAllProduct() {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products', options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetAllCategories() {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Category', options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    GetAllProductPageList(pageno, pagesize) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Products' + '/' + pageno + '/' + pagesize, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    public AddProduct(ProductModel: Product) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.post(this.apiUrl + 'Products', ProductModel, options)
            .map((res: Response) => res.json())
            .catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['Login']);
                }
                return response;
            });
    }

    GetCategorybyId(id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.get(this.apiUrl + 'Category' + '/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    public AddCategory(CategoryModel: Category) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.post(this.apiUrl + 'Category', CategoryModel, options)
            .map((res: Response) => res.json())
            .catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['Login']);
                }
                return response;
            });
    }

    public UpdateFragrance(FragranceModel: ProductFragrance) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.post(this.apiUrl + 'Fragrance', FragranceModel, options)
            .map((res: Response) => res.json())
            .catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['Login']);
                }
                return response;
            });
    }

    DeleteProduct(id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.delete(this.apiUrl + 'Products' + '/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    DeleteCategory(id: string) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.delete(this.apiUrl + 'Category' + '/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

    DeleteFragrance(id: number) {
        const headers = new Headers({ 'Content-Type': 'application/json' });
        const options = new RequestOptions('{ headers: headers }');
        return this.http.delete(this.apiUrl + 'Fragrance' + '/' + id, options).
            map((response: Response) => {
                const webresponse = response.json() && response.json();
                return webresponse;
            }
            ).catch(response => {
                if (response.status === 401) {
                    this.route.navigate(['match-list']);
                }
                return response;
            });
    }

}


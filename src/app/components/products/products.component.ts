import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormArray, FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Product, ProductFragrance } from './models/product.model';
import { ProductsService } from './services/product.service';
import {ToastyService, ToastyConfig, ToastOptions, ToastData} from 'ng2-toasty';
import { ActivatedRoute, Router } from '@angular/router';
import { AppConfig } from '../_config/app.config';
import { DialogComponent } from '../dialog/dialog.component';
import { MatDialog, MatTableDataSource } from '@angular/material';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.css'],
  providers: [ProductsService, ToastyService, ToastyConfig]
})
export class ProductsComponent implements OnInit {
  @ViewChild('fileInput') myFileInput: ElementRef;

  fileUrl: string;
  selecetdFile: File;
  imagePreview: string;
  ImageURL = new AppConfig().ImageUrl;
  Product: Product = new Product();
  ProductFragrances: Array<ProductFragrance> = [];
  urls = [];
  urlsPost = [];
  displayedColumns: string[] = ['position', 'image', 'name', 'action'];
  loading = false;
  ID: string;
  ProductFragrance: any;
  categories: any;
  dataSource = new MatTableDataSource(this.ProductFragrances);
  constructor(
    private fb: FormBuilder,
    private productService: ProductsService,
    private toastyService: ToastyService,
    private toastyConfig: ToastyConfig,
    private route: ActivatedRoute,
    private router: Router,
    public dialog: MatDialog
    ) {
      this.toastyConfig.theme = 'material';
      this.toastyConfig.position = 'center-center';
    }

  ngOnInit() {
    this.ID = this.route.snapshot.paramMap.get('ID');
    this.loading = true;
    this.productService.GetAllCategories().subscribe(
      data => {
        this.loading = false;
        this.categories = data;
      }
    );
    if (this.ID != null) {
      this.editProduct();
    }
  }

  editProduct() {
    console.log(this.ID);
    this.Product.productFragranceName =  [];
    this.productService.GetProductbyId(this.ID).subscribe(
      dataProduct => {
        this.Product = dataProduct;
      }
    );
    this.productService.GetFragranceProductbyId(this.ID).subscribe(
      data => {
        this.ProductFragrance = data;
        this.ProductFragrance.forEach(element => {
          this.ProductFragrances.push(element);
          this.dataSource =  new MatTableDataSource(this.ProductFragrances);
        });
      }
    );
    console.log(this.ProductFragrances);
    console.log(this.urls);
  }

  onSubmit() {
    if (this.ID == null) {
      this.ProductFragrances.forEach(element => {
        this.Product.productFragranceImage.push(element.fileName.toString().split(',')[1]);
        this.Product.productFragranceName.push(element.fragranceName);
      });
      console.log(this.Product);
      this.loading = true;
      this.productService.AddProduct(this.Product).subscribe(
        data => {
          this.loading = false;
          this.toastyService.success('Product Added');
          this.router.navigate(['/product-view']);
        }
      );
    } else {
      this.productService.AddProduct(this.Product).subscribe(
        data => {
          this.loading = false;
          this.toastyService.success('Product Updated');
          this.router.navigate(['/product-view']);
        }, error => {
          this.loading = false;
          this.toastyService.error('Failed');
        }
      );
    }
  }

  fragranceEdit(productFragrance: ProductFragrance) {
    this.loading = true;
    let obj: ProductFragrance = new ProductFragrance();
    obj = productFragrance;
    if (productFragrance.productId == null) {
      productFragrance.productId = this.ID;
      productFragrance.isDeleted = false;
      productFragrance.isActive = true;
    }
    console.log(productFragrance);
    this.productService.UpdateFragrance(productFragrance).subscribe(
      data => {
        this.loading = false;
        this.toastyService.success('Fragrance Updated');
      }, error => {
        this.loading = false;
        this.toastyService.error('Failed');
      }
    );
  }

  deleteItem(i: any, Id: any) {
    this.ProductFragrances.splice(i, 1);
    this.dataSource = new MatTableDataSource(this.ProductFragrances);
    if (Id != null) {
      this.loading = true;
      this.productService.DeleteFragrance(Id).subscribe(
        data => {
          this.loading = false;
          this.toastyService.success('Fragrance Updated');
        }, error => {
          this.loading = false;
          this.toastyService.error('Failed');
        }
      );
    }
  }

  openDialog(IsAdd: boolean, Fragrance?: ProductFragrance) {
    // console.log(Id);
    // tslint:disable-next-line:prefer-const
    let dialogRef = this.dialog.open(DialogComponent, {
        width: '80vw',
        data: {
            fragrance: Fragrance, isAdd: IsAdd
        }
    });


    dialogRef.afterClosed().subscribe(result => {
        // console.log('The dialog was closed');
        // this.No = result;
        // if (result === 'Yes') {
        //     this.DeleteRecored(this.tempId);
        // }
        if (result != null) {
          console.log(result);
          this.ProductFragrances.push(result);
          this.dataSource = new MatTableDataSource(this.ProductFragrances);
          console.log(this.ProductFragrances);
        }
    });
}

}

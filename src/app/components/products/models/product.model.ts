export class Product {
    id: number;
    productId: string;
    categoryId: string;
    productName: string;
    itemCode: string;
    package: number;
    specification1: string;
    specification2: string;
    specification3: string;
    fullDescription: string;
    useCase: string;
    productFragranceImage = [];
    productFragranceName = [];
    hostIp: string;
    createdBy: string;
    createdDate: Date;
    updatedBy: string;
    updatedDate: Date;
    isActive: boolean;
    isDeleted: boolean;
}

export class Category {
    id: number;
    categoryId: string;
    categoryName: string;
    hostIp: string;
    createdBy: string;
    createdDate: Date;
    updatedBy: string;
    updatedDate: Date;
    isActive: boolean;
    isDeleted: boolean;
}

export class ProductFragrance {
    id: number;
    productId: string;
    fragranceName: string;
    fileName: string;
    fileSize: string;
    fileType: string;
    hostIp: string;
    createdBy: string;
    createdDate: Date;
    updatedBy: string;
    updatedDate: Date;
    isActive: boolean;
    isDeleted: boolean;
}
